from modeltranslation.translator import translator, TranslationOptions
from .models import FlatPage


class PageTranslationOptions(TranslationOptions):
    fields = ('title', 'text', )


translator.register(FlatPage, PageTranslationOptions)
