# -*- coding: utf-8 -*-
from django.contrib import admin

from common.admin import TranslationTabs

from .models import FlatPage


@admin.register(FlatPage)
class PageAdmin(TranslationTabs):
    list_display = ('title', 'url', )
    list_editable = ('url', )
    search_fields = ('url', 'title',)
