from urllib.parse import unquote

from django.conf import settings
from django.views.decorators.cache import cache_page
from django.utils.translation import get_language
from django.contrib.humanize.templatetags.humanize import intcomma
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.urls import translate_url
from django.template.response import TemplateResponse
from django.db.models import Count, Prefetch, Q
from django.utils.decorators import method_decorator
from django.utils.translation import (LANGUAGE_SESSION_KEY, check_for_language, )
from django.views.generic import DetailView, ListView, View, TemplateView
from django.utils.translation import ugettext_lazy as _

from .models import Category, Tag, Game, Block
from .utils import mobile_order, require_ajax


class TagList(ListView):
    context_object_name = "objects"
    template_name = "catalog/tags.html"

    def get_queryset(self, *args, **kwargs):
        lang = get_language()
        exclude = {'name_{lang}__isnull'.format(lang=lang.replace('-', '_')): True}
        order_field = 'name_%s' % lang

        games = mobile_order(Game.published, self.request)
        objects = Tag.published.all().prefetch_related(
            Prefetch('game_set', games)
        ).annotate(
            total_games=Count('game', filter=Q(game__public=True), exclude=Q(**exclude)),
        ).order_by(
            'position', order_field
        )
        return objects


def game_list_by_category(request, slug):
    object = get_object_or_404(Category.published, slug=slug)
    sort = request.GET.get('sort')
    if sort == 'new':
        order = '-created'
    elif sort == 'name':
        order = 'name'
    else:
        order = '-play_counter'
    entry_list = mobile_order(object.games_published.all(), request, order)
    return TemplateResponse(request, 'catalog/category.html', locals())


def game_list_by_tag(request, slug):
    object = get_object_or_404(Tag.published, slug=slug)
    sort = request.GET.get('sort')
    if sort == 'new':
        order = '-created'
    elif sort == 'name':
        order = 'name'
    else:
        order = '-play_counter'
    entry_list = mobile_order(object.games_published.all(), request, order)
    return TemplateResponse(request, 'catalog/tag.html', locals())


def game_list(request, action):
    if action == 'new':
        order = '-created'
        title = _('New games - Free Games Boom.com')
        h1 = _('New games')
    else:
        order = '-play_counter'
        title = _('Popular games - Free Games Boom.com')
        h1 = _('Popular games')
    objects = mobile_order(Game.published.all(), request, order)
    return TemplateResponse(request, 'catalog/list.html', locals())


class GameDetail(DetailView):
    model = Game
    queryset = Game.published.all()
    template_name = "catalog/game.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.insecure:
            raise Http404
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(GameDetail, self).get_context_data(**kwargs)
        self.set_last_open()
        game = context['object']
        priority_tag = game.priority_tag
        if priority_tag:
            games_by_priority_tag = Game.objects.filter(tags=priority_tag).exclude(id=game.id).order_by('-play_counter')[:16]
            priority_title = _('Similar %s games' % priority_tag.name)
            context['games_by_priority_tag'] = games_by_priority_tag
            context['priority_title'] = priority_title
        return context

    def set_last_open(self):
        last_played_session = self.request.session.get('last_played')
        if not last_played_session:
            # create new session
            self.request.session['last_played'] = [self.object.pk]
        else:
            # add to begin of exist session and remove duplicates
            last_played_session.insert(0, self.object.pk)
            self.request.session['last_played'] = list(dict.fromkeys(last_played_session))
        self.request.session.modified = True


class GameDetailFull(DetailView):
    model = Game
    queryset = Game.published.all()
    template_name = "catalog/game_full.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.insecure:
            raise Http404
        self.object.play_counter_mobile += 1
        self.object.save()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class GameDetailNoSecure(GameDetail):

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.insecure:
            raise Http404
        elif self.request.scheme == 'https':
            return redirect('http://%s%s' % (self.request.get_host(), self.object.get_absolute_url()), permanent=True)
        self.object.play_counter_mobile += 1
        self.object.save()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class GameDetailNoSecureFull(GameDetailFull):
    template_name = "catalog/game_full.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.insecure:
            raise Http404
        elif self.request.scheme == 'https':
            return redirect('http://%s%s' % (self.request.get_host(), self.object.get_absolute_url_full()), permanent=True)
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class SearchView(View):
    template_name = 'catalog/search.html'

    def get(self, request, *args, **kwargs):
        q = request.GET.get('q')
        if not q:
            raise Http404
        qs = Game.published.filter(
            Q(name_en__icontains=q) | Q(name_ru__icontains=q) | Q(name_ar__icontains=q) | Q(slug__icontains=q)
        )
        objects = mobile_order(qs, self.request)
        return render(request, self.template_name, {'objects': objects})


class DoestWorkAjax(View):
    def post(self, request, pk):
        if request.is_ajax():
            data = dict()
            game = get_object_or_404(Game.published, pk=pk)
            game.doesnt_work += 1
            game.save()
            return JsonResponse(data)
        else:
            return HttpResponseForbidden()


class PopupGamesAjax(TemplateView):
    template_name = 'catalog/inclusions/popup-games.html'

    @method_decorator(require_ajax)
    def post(self, request,  action='favourites', *args, **kwargs):
        objects = None
        if action == 'favourites':
            title = _('Favourite games')
            game_ids = request.session.get('favourites')
        else:
            title = _('Last Played')
            game_ids = request.session.get('last_played')

        if game_ids:
            # qs = Game.published.filter(id__in=game_ids).extra(
            #     select={'manual': "FIELD(id, %s)" % ','.join(str(x) for x in game_ids)}, order_by=['manual']
            # ).order_by('manual').distinct()
            qs = Game.published.filter(id__in=game_ids).order_by('manual').distinct()
            objects = mobile_order(qs, request)

        return render_to_response(self.template_name, {
            'title': title, 'objects': objects, 'request': request, 'action': action
        })


class AddToFavouritesAjax(View):

    @method_decorator(require_ajax)
    def post(self, request, pk):
        data = dict()
        game = get_object_or_404(Game.published, pk=pk)

        favourites_session = self.request.session.get('favourites')
        if not favourites_session:
            # create new session
            self.request.session['favourites'] = [game.pk]
        else:
            # add to begin of exist session and remove duplicates
            favourites_session.insert(0, game.pk)
            self.request.session['favourites'] = list(dict.fromkeys(favourites_session))
        self.request.session.modified = True
        data['total'] = len(self.request.session['favourites'])
        data['text'] = _('Added')
        return JsonResponse(data)


class LikeDislikeAjax(View):
    def post(self, request, pk, action):
        data = dict()
        game = get_object_or_404(Game.published, pk=pk)

        likes_session = self.request.session.get('likes')
        if not likes_session or game.id not in likes_session:
            if action == 'like':
                game.like += 1
                game.fakelike += 1
                data['total'] = intcomma(game.fakelike)
            else:
                game.dislike += 1
                game.fakedislike += 1
                data['total'] = intcomma(game.fakedislike)
            data['total_votes'] = intcomma(game.total_votes)
            data['rating'] = game.rating
            game.save()

            #  пишем в сессию
            if 'likes' not in self.request.session or not self.request.session.get('likes'):
                self.request.session['likes'] = [game.pk]
            else:
                likes_list = self.request.session.get('likes')
                likes_list.insert(0, game.pk)
                self.request.session['likes'] = likes_list
        print(data)
        return JsonResponse(data, safe=False)


class EmbedCodeAjax(View):
    @method_decorator(require_ajax)
    def post(self, request, pk):
        game = get_object_or_404(Game.published, pk=pk)
        if request.user_agent.is_mobile or request.user_agent.is_tablet:
            game.play_counter_mobile += 1
        else:
            game.play_counter += 1
        game.save()
        resp = JsonResponse({'code': game.embed_code}, safe=False)
        return resp


class ClearSessionAction(View):

    @method_decorator(require_ajax)
    def post(self, request, action):
        name = 'favourites' if action == 'favourites' else 'last_played'
        try:
            self.request.session[name] = []
        except KeyError:
            pass
        self.request.session.modified = True
        return JsonResponse({'action': action})


class AjaxInfo(View):

    @method_decorator(require_ajax)
    def post(self, request):
        data = {}
        if self.request.session.get('favourites'):
            data['favourites_total'] = len(self.request.session.get('favourites'))
        if self.request.session.get('favourites'):
            data['last_played_total'] = len(self.request.session.get('last_played'))
        return JsonResponse(data)


@require_ajax
def autocomplete_search(request):
    q = request.GET.get('term')
    if q:
        search_qs = Game.published.filter(
            Q(name_en__icontains=q) | Q(name_ru__icontains=q) | Q(name_ar__icontains=q) | Q(slug__icontains=q)
        )
        results = []
        for obj in mobile_order(search_qs, request):
            results.append({'value': obj.name, 'url': '%s://%s%s' % (
                obj.scheme, request.get_host(), obj.get_absolute_url()
            )})
        return JsonResponse(results, safe=False)
    else:
        raise Http404


LANGUAGE_QUERY_PARAMETER = 'language'


def set_language(request):
    """
    Redirect to a given URL while setting the chosen language in the session
    (if enabled) and in a cookie. The URL and the language code need to be
    specified in the request parameters.

    Since this view changes how the user will see the rest of the site, it must
    only be accessed as a POST request. If called as a GET request, it will
    redirect to the page in the request (the 'next' parameter) without changing
    any state.
    """
    next = request.POST.get('next', request.GET.get('next'))
    if next or not request.is_ajax():
        next = request.META.get('HTTP_REFERER')
        next = next and unquote(next)  # HTTP_REFERER may be encoded.
    response = HttpResponseRedirect(next) if next else HttpResponse(status=204)
    if request.method == 'POST':
        lang_code = request.POST.get(LANGUAGE_QUERY_PARAMETER)
        if lang_code and check_for_language(lang_code):
            if next:
                next_trans = translate_url(next, lang_code)
                if next_trans != next:
                    response = HttpResponseRedirect(next_trans)
            if hasattr(request, 'session'):
                request.session[LANGUAGE_SESSION_KEY] = lang_code
            response.set_cookie(
                settings.LANGUAGE_COOKIE_NAME, lang_code,
                max_age=settings.LANGUAGE_COOKIE_AGE,
                path=settings.LANGUAGE_COOKIE_PATH,
                domain=settings.LANGUAGE_COOKIE_DOMAIN,
            )
    return response
