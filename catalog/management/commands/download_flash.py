# -*- coding: utf-8 -*-
import os
import tempfile
import datetime
import requests

from tqdm import tqdm

from django.core import files
from django.core.management.base import BaseCommand

from catalog.models import Game


class Command(BaseCommand):

    def handle(self, *args, **options):

        objects = tqdm(Game.objects.filter(
            agame=True,
            flash_link__contains='.swf',
            created__gte=datetime.datetime.today().replace(hour=0, minute=0, second=0)
        ))

        for game in objects:

            # Steam the image from the url
            try:
                if not game.flash:

                    url = game.flash_link
                    try:
                        r = requests.get(url, stream=True, allow_redirects=True, timeout=3)
                    except (requests.exceptions.SSLError, requests.exceptions.ConnectionError):
                        url = game.flash_link.replace('https', 'http')
                        try:
                            r = requests.get(url, stream=True, allow_redirects=True, timeout=3)
                        except requests.exceptions.ConnectionError:
                            continue

                    # Was the request OK?
                    if r.status_code != requests.codes.ok:
                        game.doesnt_work += 1
                        continue

                    # Create a temporary file
                    lf = tempfile.NamedTemporaryFile()

                    # Read the streamed image in sections
                    for block in r.iter_content(1024 * 8):

                        # If no more file then stop
                        if not block:
                            break

                        # Write image block to temporary file
                        lf.write(block)

                    game.flash.save(
                        os.path.basename(url),
                        files.File(lf)
                    )
                    game.save()
            except:
                pass
