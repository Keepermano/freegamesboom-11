# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
from catalog.models import Import, Game
import pandas as pd


class Command(BaseCommand):

    def handle(self, *args, **options):
        objects = Import.objects.filter(success=False, in_processed=False)

        for o in objects:
            total = 0
            df = pd.read_excel('/srv/freegamesboom.com' + o.file.url)
            o.in_processed = True
            o.save()
            for k, v in df.iterrows():
                try:
                    obj = Game.objects.get(pk=v['id'])
                except Game.DoesNotExist:
                    pass
                else:
                    obj.name_ru = v['name_ru']
                    obj.name_ru = v['name_ar']
                    obj.name_ru = v['name_en']
                    obj.save()
                    total += 1
            o.success = True
            o.in_processed = False
            o.message = 'Updated: %d' % total
            o.save()
