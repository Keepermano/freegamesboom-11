# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < "
"11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? "
"1 : n % 1 != 0 ? 2: 3);\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "Failo tipas nepalaikomas."

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "Nemokami zaidimai BOOM"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "Raskite daugiau 100000 zaidimu"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "Paieska"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "Nauja"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "Populiarios"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "Megstamiausi <br>zaidimai"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "Paskutini <br>karta grojo"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "Informacija"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "Kategorija"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "Zaidimas"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "ZAISTI"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "Sis zaidimas negalimas mobiliajame telefone."

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "Valdikliai"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "Vaizdo irasas"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "Zaisti!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "TOP KATEGORIJOS"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "balsu"

#: catalog/templates/catalog/inclusions/game_block.html:11
#, fuzzy
#| msgid "Doesn't  <br>work?"
msgid "Doesn’t  <br>work?"
msgstr "<br>Neveikia?"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "Prideta"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "Itraukti i <br>adresyna"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "Dalintis"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "Per visa ekrana"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "Svarus sarasas"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "Rusiuoti pagal"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "Populiariausias"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "Zymos"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "Populiarios zymes"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "POPULIARIAUSIAI ZAIDIMAI"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "LIAUDIES ZENKLAI"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "POPULIARIAUSIAS"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "FGM REKOMENDUOJAMA!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "DAUGIAU %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "PASKUTINES ZAIDIMAI"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "Nauji zaidimai"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "Nauji zaidimai - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "Populiarus zaidimai - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "Populiarus zaidimai"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "Populiarus zaidimai"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "Megstamiausi zaidimai"

#: catalog/views.py:194
msgid "Last Played"
msgstr "Paskutini karta zaide"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "Plokstieji puslapiai"

#: flatpages/models.py:12
msgid "Title"
msgstr "Pavadinimas"

#: flatpages/models.py:14
msgid "Text"
msgstr "Tekstas"

#: flatpages/models.py:15
msgid "Active"
msgstr "Aktyvus"

#: flatpages/models.py:16
msgid "Modified"
msgstr "Modifikuotas"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "Statiniai puslapiai"

#: flatpages/models.py:20
msgid "Static page"
msgstr "Statinis puslapis"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "Afrikieciu"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "Arabiskas"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "Azerbaidzanieciu"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "Bulgaru"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "Baltarusiu"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "Bengalu"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "Bosnijos"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "Katalonu"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "Cekija"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "Velso"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "Danijos"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "Vokietija"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "Graikijos"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "Anglu"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "Esperanto"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "Ispanu"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "Estijos"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "Suomiu"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "Prancuzijos"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "Airijos"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "Galisu"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "Hebraju"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "Hindi"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "Kroatu"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "Vengru"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "Armenu"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "Italijos"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "Japonijos"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "Gruzinu"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "Kazachu"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "Khmeru"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "Kannada"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "Korejos"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "Liuksemburgieciu"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "Lietuvos"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "Latvijos"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "Makedonijos"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "Malayalam"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "Mongolu"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "Marati"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "Birmos"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "Nepali"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "Olandu"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "Osetija"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "Punjabi"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "Lenkijos"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "Portugalu"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "Rumunu"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "Rusijos"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "Slovakija"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "Slovenija"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "Albanas"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "Serbu"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "Svedijos"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "Svahili"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "Tamilu"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "Telugu"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "Taju"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "Turkijos"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "Totoriu"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "Ukrainos"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "Urdu"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "Vietnamieciu"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "Supaprastinta kinu kalba"

#: templates/404.html:18
msgid "404 Error"
msgstr "404 klaida"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "Oi. Sio puslapio nera."

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "daugiau"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "Visi"

#~ msgid "Indonesian"
#~ msgstr "Indonezieciu"

#~ msgid "Kabyle"
#~ msgstr "Kabyle"

#~ msgid "Udmurt"
#~ msgstr "Udmurtas"
