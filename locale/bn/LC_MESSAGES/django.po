# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "ফাইল টাইপ সমর্থিত নয়।"

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "বিনামূল্যে গেম BOOM"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "আরও 100,000 গেমস সন্ধান করুন"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "অনুসন্ধান করুন"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "নতুন"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "জনপ্রিয়"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "প্রিয় <br> গেমস"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "শেষ <br> খেলা হয়েছে"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "তথ্য"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "বিভাগ"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "খেলা"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "প্লে"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "এই গেমটি মোবাইলে উপলভ্য নয়।"

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "নিয়ন্ত্রণ"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "ভিডিও"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "খেলুন!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "শীর্ষ ক্যাটাগরি"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "ভোট"

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr "<br> কাজ করে না?"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "যোগ করা হয়েছে"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "<br> প্রিয়গুলিতে যুক্ত করুন"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "ভাগ"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "পূর্ণ স্ক্রীন"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "পরিষ্কার তালিকা"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "অনুসারে বাছাই করুন"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "সর্বাধিক জনপ্রিয়"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z ক্রমানুসারে"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "ট্যাগ্স"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "জনপ্রিয় ট্যাগ"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "সর্বাধিক জনপ্রিয় গেমস"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "জনপ্রিয় ট্যাগস"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "সর্বাধিক জনপ্রিয়"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "এফজিএম সুপারিশ!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "আরও %(category)s গুলি"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "সর্বশেষ খেলেছে"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "নতুন গেমস"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "নতুন গেমস - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "জনপ্রিয় গেমস - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "জনপ্রিয় গেমস"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "বিভাগ থেকে অনুরূপ গেমস %s"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "প্রিয় গেমস"

#: catalog/views.py:194
msgid "Last Played"
msgstr "শেষবার খেলেছি"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "সমতল পৃষ্ঠাগুলি"

#: flatpages/models.py:12
msgid "Title"
msgstr "উপাধি"

#: flatpages/models.py:14
msgid "Text"
msgstr "পাঠ"

#: flatpages/models.py:15
msgid "Active"
msgstr "সক্রিয়"

#: flatpages/models.py:16
msgid "Modified"
msgstr "সংশোধিত"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "স্থির পৃষ্ঠাগুলি"

#: flatpages/models.py:20
msgid "Static page"
msgstr "স্থির পৃষ্ঠা"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "আফ্রিকান্স"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "আরবি"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "আজারবাইজানীয়"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "বুলগেরীয়"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "বেলারুশিয়"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "বাঙালি"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "বসনীয়"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "কাতালান"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "চেক"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "ওয়েলশ"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "ডেনমার্কের"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "জার্মান"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "গ্রিক"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "ইংরেজি"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "এস্পেরান্তো"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "স্পেনীয়"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "এস্তোনীয়"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "ফিনিশ"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "ফরাসি"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "আইরিশ"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "গ্যালিশিয়"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "হিব্রু"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "হিন্দি"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "ক্রোয়েশীয়"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "হাঙ্গেরীয়"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "আর্মেনিয়"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "ইতালীয়"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "জাপানি"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "জর্জিয়ান"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "কাজাখ"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "খেমের"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "কন্নড"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "কোরিয়ান"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "লুক্সেমবার্গীয়"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "লিথুয়েনীয"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "লাত্ভীয়"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "ম্যাসেডোনীয"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "মালায়ালম"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "মঙ্গোলিয়"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "মারাঠি"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "বর্মী"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "নেপালি"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "ডাচ"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "ওসেটিক"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "পাঞ্জাবি"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "পোলিশ"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "পর্তুগীজ"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "রোমানিয়ন"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "রাশিয়ান"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "স্লোভাক"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "স্লোভেনীয়"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "আলবেনীয়"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "সার্বীয়"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "সুইডিশ"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "সোয়াহিলি"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "তামিল"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "তেলুগু"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "থাই"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "তুর্কী"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "তাতারদের"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "ইউক্রেনীয়"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "উর্দু"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "ভিয়েতনামী"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "সরলীকৃত চীনা"

#: templates/404.html:18
msgid "404 Error"
msgstr "404 ত্রুটি"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "উফ। এই পৃষ্ঠার অস্তিত্ব নেই।"

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "অধিক"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "সব"

#~ msgid "Indonesian"
#~ msgstr "ইন্দোনেশিয়াসম্বন্ধীয়"

#~ msgid "Kabyle"
#~ msgstr "কাবাইলে"

#~ msgid "Udmurt"
#~ msgstr "উডমুর্ট"
